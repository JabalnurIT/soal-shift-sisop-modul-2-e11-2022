# **Laporan Penjelasan Soal Shift Modul 2**
## Sistem Operasi E 2022
### **Kelompok E-11**
**Asisten : Dewangga Dharmawan**  
**Anggota :**
- Jabalnur 5025201241
- Vania Rizky Juliana Wachid 5025201215
- Maula Izza Azizi 5025201104  

## **Daftar Isi Laporan**
- [Soal 1](#soal-1)
- [Soal 2](#soal-2)
- [Soal 3](#soal-3)
- [Kendala](#kendala)

# Soal 1
## **Membuat Exec Untuk Mendownload File Zip** ##
Pada poin a, program kita diharapkan mampu melakukan download file characters dan file weapons dengan menggunakan exec.
### Download File Characters ###
Berikut adalah kode untuk dapat melakukan download file characters dari alamat google drive
```c
char *argv[] = {"wget","-O","./file1.zip","https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp","-qq",NULL};
execv("/usr/bin/wget", argv);
```
### Download File Weapons ###
Berikut adalah kode untuk dapat melakukan download file weapons dari alamat google drive
```c
char *argv[] = {"wget","-O","./file2.zip","https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT","-qq",NULL};
execv("/usr/bin/wget", argv);
```

## **Membuat Exec Untuk Mengekstrak File Zip** ##
Selanjutnya, program kita akan melakukan ekstrak terhadap file zip yang telah didownload sebelumnya dengan menggunakan exec.
### Ekstrak File Characters ###
Berikut adalah kode untuk dapat melakukan ekstrak terhadap isi file characters
```c
char *argv[] = {"unzip","-qq","file1.zip", NULL};
execv("/bin/unzip", argv);
```
### Ekstrak File Weapons ###
Berikut adalah kode untuk dapat melakukan ekstrak terhadap isi file weapons
```c
char *argv[] = {"unzip","-qq","file2.zip", NULL};
execv("/bin/unzip", argv);
```

## **Membuat Exec Untuk Membuat Folder Gacha-gacha** ##
Selanjutnya, program kita akan membuat folder baru dengan nama gacha-gacha yang akan digunakan sebagai working directory dari program kita.
Berikut adalah kode untuk dapat membuat folder baru gacha-gacha
```c
char *argv[] = {"mkdir","-p", "./gacha-gacha", NULL};
execv("/usr/bin/mkdir", argv);
```

## **Membuat Exec Untuk Menghapus File Zip yang sudah Diekstrak** ##
Selanjutnya, program kita akan menghapus kedua file zip sebelumnya yang sudah diekstrak.
Berikut adalah kode untuk dapat menghapus file tersebut
```c
char *argv[] = {"rm", "./file1.zip", "./file2.zip", NULL};
execv("/usr/bin/rm", argv);
```

## **Menggabung Seluruh Exec dengan menggunakan Fork** ##
Setelah seluruh perintah dari soal1 bagian a terpenuhi, kita akan menggabungkan keseluruhan kodennya dengan menggunakan fork karena exec akan menghentikan jalannya program setelah perintah execnya berjalan
```c
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<stdio.h>
#include<wait.h>
#include<string.h>
#include<pwd.h>
#include<sys/stat.h>
#include<dirent.h>
#include<json-c/json.h>
#include<time.h>

int main(int argc, char **argv)
{
    int status;
    pid_t child_id;
    child_id = fork();
    if(child_id < 0) exit(EXIT_FAILURE);
    else if (child_id == 0)
    {
        int status;
        pid_t child_id;
        child_id = fork();
        if(child_id < 0) exit(EXIT_FAILURE);
        else if (child_id == 0)
        {
            int status;
            pid_t child_id;
            child_id = fork();
            if(child_id < 0) exit(EXIT_FAILURE);
            else if (child_id == 0)
            {
                int status;
                pid_t child_id;
                child_id = fork();
                if(child_id < 0) exit(EXIT_FAILURE);
                else if (child_id == 0)
                {
                    int status;
                    pid_t child_id;
                    child_id = fork();
                    if(child_id < 0) exit(EXIT_FAILURE);
                    else if (child_id == 0)
                    {
                        int status;
                        pid_t child_id;
                        child_id = fork();
                        if(child_id < 0) exit(EXIT_FAILURE);
                        else if (child_id == 0)
                        {
                            char *argv[] = {"wget","-O","./file1.zip","https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp","-qq",NULL};
                            execv("/usr/bin/wget", argv);
                        }
                        else
                        {
                            while((wait(&status))>0);
                            char *argv[] = {"wget","-O","./file2.zip","https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT","-qq",NULL};
                            execv("/usr/bin/wget", argv);
                        }
                    }
                    else
                    {
                        while((wait(&status))>0);
                        char *argv[] = {"unzip","-qq","file1.zip", NULL};
                        execv("/bin/unzip", argv);
                    }
                }
                else
                {
                    while((wait(&status))>0);
                    char *argv[] = {"unzip","-qq","file2.zip", NULL};
                    execv("/bin/unzip", argv);
                }
            }
            else 
            {
                while((wait(&status))>0);
                char *argv[] = {"mkdir","-p", "./gacha-gacha", NULL};
                execv("/usr/bin/mkdir", argv);
            }
        }
        else 
        {
            while((wait(&status))>0);
            char *argv[] = {"rm", "./file1.zip", "./file2.zip", NULL};
            execv("/usr/bin/rm", argv);
        }
    }
    else 
    {
        while((wait(&status))>0);
        // Lanjutan Kode kita, akan dituliskan disini
    }
}
```
Berdasarkan kode diatas, parent prosesnya akan berjalan setelah child prosesnya berjalan, dimana hal itu akan terjadi, karene child prosessnya telah berhenti akibat dari execnya. Sedangkan yang membuat parentnya baru dapat berjalan setelah childnya berhenti karena adanya fungsi wait(&status) yang akan menunggu status dari childnya bernilai 0.

## **Mengambil Seluruh Nama isi Dari Database Characters dan Weapons** ##
Selanjutnya, pada soal bagian b, program kita akan mengambil seluruh nama dari Database Characters dan Weapons dan disimpan kedalam sebuah file txt agar nantinya dapat digacha secara random.
### Membuat File characters.txt dan weapons.txt  ###
Berikut adalah kode untuk dapat membuat file.txt untuk menaruh daftar nama-nama file di dalam database
```c
DIR *dp;
                
struct dirent *ep;
char path[100];
int n=0,m=0;
srand(time(0));

FILE *fp;
fp=fopen("./characters/characters.txt","w");
strcpy(path,"./characters");
dp = opendir(path);
if (dp != NULL)
{
    while ((ep = readdir (dp))) {
        if(strlen(ep->d_name)>2) fprintf(fp,"%s\n",ep->d_name);
    }
    (void) closedir (dp);
} else perror ("Couldn't open the directory");
fclose(fp);

fp=fopen("./weapons/weapons.txt","w");
strcpy(path,"./weapons");
dp = opendir(path);
if (dp != NULL)
{
    while ((ep = readdir (dp))) {
        if(strlen(ep->d_name)>2) fprintf(fp,"%s\n",ep->d_name);
    }
    (void) closedir (dp);
} else perror ("Couldn't open the directory");
fclose(fp);
```

### Menyimpan Nama dari isi Database ke Dalam File characters.txt dan weapons.txt ###
Berikut adalah kode untuk menyimpan daftar nama file yang ada dalam database ke dalam file.txt
```c
fp = fopen("./characters/characters.txt","r");	
int i = 0; 
while(1){ 
    char r = (char)fgetc(fp); 
    int k = 0; 
    while(r!='\n' && !feof(fp))
    {
        characters[i][k++] = r;			
        r = (char)fgetc(fp); 
    } 
    characters[i][k]=0;		
    if(feof(fp))
    {		 
        break; 
    } 
    i++; 
} 
fclose(fp);
fp = fopen("./weapons/weapons.txt","r");		
i = 0; 
while(1){ 
    char r = (char)fgetc(fp); 
    int k = 0; 
    while(r!='\n' && !feof(fp))
    {	
        weapons[i][k++] = r;			
        r = (char)fgetc(fp); 
    } 
    weapons[i][k]=0;		
    if(feof(fp))
    {		
        break; 
    } 
    i++; 
} 
fclose(fp);
```
## **Membuat Percabangan Saat Proses Gacha** ##
Setelah menyimpan isi dari database kita ke file.txt, kita akan membuat aturan percabangan dari gachanya.
### Gacha Ganjil Ambil Item Characters ###
Kita membuat percabangan apabila jumlah gachanya adalah ganjil, maka akan mengambil item Characters
```c
int jmlgacha=0;

char characters[48][100];
char weapons[130][100];

while(1)
{
    jmlgacha++;
    if(jmlgacha&1)
    {
        strcpy(tipeItem,"characters");
        strcpy(namaItem,"./");
        strcat(namaItem,tipeItem);
        strcat(namaItem,"/");
        strcat(namaItem,characters[0]);
        fp = fopen(namaItem,"r");
        fread(buffer, 10240, 1, fp);
        fclose(fp);

        parsed_json = json_tokener_parse(buffer);
    }
}
```
### Gacha Genap Ambil Item Weapons ###
Kemudian kita menambahkan percabangan apabila jumlah gachanya adalah genap, maka akan mengambil item Weapons
```c
int jmlgacha=0;

char characters[48][100];
char weapons[130][100];

while(1)
{
    jmlgacha++;
    if(jmlgacha&1)
    {
        strcpy(tipeItem,"characters");
        strcpy(namaItem,"./");
        strcat(namaItem,tipeItem);
        strcat(namaItem,"/");
        strcat(namaItem,characters[0]);
        fp = fopen(namaItem,"r");
        fread(buffer, 10240, 1, fp);
        fclose(fp);

        parsed_json = json_tokener_parse(buffer);
    }else
    {
        strcpy(tipeItem,"weapons");
        strcpy(namaItem,"./");
        strcat(namaItem,tipeItem);
        strcat(namaItem,"/");
        strcat(namaItem,weapons[0]);
        fp = fopen(namaItem,"r");
        fread(buffer, 10240, 1, fp);
        fclose(fp);

        parsed_json = json_tokener_parse(buffer);
    }
}
```
### Gacha Mod 10 Membuat File dan menyimpan Hasil Gacha Sebelumnya ###
Kemudian kita membuat percabangan apabila jumlah gachanya adalah mod 10, maka akan dibuat sebuah file txt baru yang akan menyimpan seluruh hasil gacha sebelumnya 
```c
int jmlgacha=0;

char characters[48][100];
char weapons[130][100];

while(1)
{
    jmlgacha++;
    if(jmlgacha&1)
    {
        strcpy(tipeItem,"characters");
        strcpy(namaItem,"./");
        strcat(namaItem,tipeItem);
        strcat(namaItem,"/");
        strcat(namaItem,characters[0]);
        fp = fopen(namaItem,"r");
        fread(buffer, 10240, 1, fp);
        fclose(fp);

        parsed_json = json_tokener_parse(buffer);
    }else
    {
        strcpy(tipeItem,"weapons");
        strcpy(namaItem,"./");
        strcat(namaItem,tipeItem);
        strcat(namaItem,"/");
        strcat(namaItem,weapons[0]);
        fp = fopen(namaItem,"r");
        fread(buffer, 10240, 1, fp);
        fclose(fp);

        parsed_json = json_tokener_parse(buffer);
    }

    if(jmlgacha%10==0)
    {
        // Kode untuk membuat File baru
    }
}
```
### Gacha Mod 90 Membuat Folder dan menyimpan Seluruh File Gacha Sebelumnya ###
Kemudian kita membuat percabangan apabila jumlah gachanya adalah mod 90, maka akan dibuat sebuah folder baru yang akan menyimpan seluruh file gacha sebelumnya 
```c
int jmlgacha=0;

char characters[48][100];
char weapons[130][100];

while(1)
{
    jmlgacha++;
    if(jmlgacha&1)
    {
        strcpy(tipeItem,"characters");
        strcpy(namaItem,"./");
        strcat(namaItem,tipeItem);
        strcat(namaItem,"/");
        strcat(namaItem,characters[0]);
        fp = fopen(namaItem,"r");
        fread(buffer, 10240, 1, fp);
        fclose(fp);

        parsed_json = json_tokener_parse(buffer);
    }else
    {
        strcpy(tipeItem,"weapons");
        strcpy(namaItem,"./");
        strcat(namaItem,tipeItem);
        strcat(namaItem,"/");
        strcat(namaItem,weapons[0]);
        fp = fopen(namaItem,"r");
        fread(buffer, 10240, 1, fp);
        fclose(fp);

        parsed_json = json_tokener_parse(buffer);
    }

    if(jmlgacha%10==0)
    {
        // Kode untuk membuat File baru dan menyimpan hasil gacha
    }
    if(jmlgacha%90==0)
    {
        // Kode untuk membuat Folder baru dan menyimpan file gacha
    }
}
```
### Membuat Pengambilan Item Secara Random ###
Selanjutnya akan dibuat pengambilan item menjadi random. angka 48 dan 130 diambil dari banyaknya isi database weapons dan characters
```c
int jmlgacha=0;
srand(time(0));

char characters[48][100];
char weapons[130][100];

while(1)
{
    jmlgacha++;
    if(jmlgacha&1)
    {
        strcpy(tipeItem,"characters");
        strcpy(namaItem,"./");
        strcat(namaItem,tipeItem);
        strcat(namaItem,"/");
        strcat(namaItem,characters[rand()%48]);
        fp = fopen(namaItem,"r");
        fread(buffer, 10240, 1, fp);
        fclose(fp);

        parsed_json = json_tokener_parse(buffer);
    }else
    {
        strcpy(tipeItem,"weapons");
        strcpy(namaItem,"./");
        strcat(namaItem,tipeItem);
        strcat(namaItem,"/");
        strcat(namaItem,weapons[rand()%130]);
        fp = fopen(namaItem,"r");
        fread(buffer, 10240, 1, fp);
        fclose(fp);

        parsed_json = json_tokener_parse(buffer);
    }

    if(jmlgacha%10==0)
    {
        // Kode untuk membuat File baru dan menyimpan hasil gacha
    }
    if(jmlgacha%90==0)
    {
        // Kode untuk membuat Folder baru dan menyimpan file gacha
    }
}
```
## **Menggabung Seluruh Kode Hingga Saat ini** ##
Setelah seluruh perintah dari soal1 bagian b terpenuhi, kita akan menggabungkan keseluruhan kode dari bagian a dan b.
```c
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<stdio.h>
#include<wait.h>
#include<string.h>
#include<pwd.h>
#include<sys/stat.h>
#include<dirent.h>
#include<json-c/json.h>
#include<time.h>

int jmlgacha=0;

char characters[48][100];
char weapons[130][100];

int main(int argc, char **argv)
{
    int status;
    pid_t child_id;
    child_id = fork();
    if(child_id < 0) exit(EXIT_FAILURE);
    else if (child_id == 0)
    {
        int status;
        pid_t child_id;
        child_id = fork();
        if(child_id < 0) exit(EXIT_FAILURE);
        else if (child_id == 0)
        {
            int status;
            pid_t child_id;
            child_id = fork();
            if(child_id < 0) exit(EXIT_FAILURE);
            else if (child_id == 0)
            {
                int status;
                pid_t child_id;
                child_id = fork();
                if(child_id < 0) exit(EXIT_FAILURE);
                else if (child_id == 0)
                {
                    int status;
                    pid_t child_id;
                    child_id = fork();
                    if(child_id < 0) exit(EXIT_FAILURE);
                    else if (child_id == 0)
                    {
                        int status;
                        pid_t child_id;
                        child_id = fork();
                        if(child_id < 0) exit(EXIT_FAILURE);
                        else if (child_id == 0)
                        {
                            char *argv[] = {"wget","-O","./file1.zip","https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp","-qq",NULL};
                            execv("/usr/bin/wget", argv);
                        }
                        else
                        {
                            while((wait(&status))>0);
                            char *argv[] = {"wget","-O","./file2.zip","https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT","-qq",NULL};
                            execv("/usr/bin/wget", argv);
                        }
                    }
                    else
                    {
                        while((wait(&status))>0);
                        char *argv[] = {"unzip","-qq","file1.zip", NULL};
                        execv("/bin/unzip", argv);
                    }
                }
                else
                {
                    while((wait(&status))>0);
                    char *argv[] = {"unzip","-qq","file2.zip", NULL};
                    execv("/bin/unzip", argv);
                }
            }
            else 
            {
                while((wait(&status))>0);
                char *argv[] = {"mkdir","-p", "./gacha-gacha", NULL};
                execv("/usr/bin/mkdir", argv);
            }
        }
        else 
        {
            while((wait(&status))>0);
            char *argv[] = {"rm", "./file1.zip", "./file2.zip", NULL};
            execv("/usr/bin/rm", argv);
        }
    }
    else 
    {
        while((wait(&status))>0);
        DIR *dp;
                
        struct dirent *ep;
        char path[100];
        int n=0,m=0;
        srand(time(0));

        FILE *fp;
        fp=fopen("./characters/characters.txt","w");
        strcpy(path,"./characters");
        dp = opendir(path);
        if (dp != NULL)
        {
            while ((ep = readdir (dp))) {
                if(strlen(ep->d_name)>2) fprintf(fp,"%s\n",ep->d_name);
            }
            (void) closedir (dp);
        } else perror ("Couldn't open the directory");
        fclose(fp);

        fp=fopen("./weapons/weapons.txt","w");
        strcpy(path,"./weapons");
        dp = opendir(path);
        if (dp != NULL)
        {
            while ((ep = readdir (dp))) {
                if(strlen(ep->d_name)>2) fprintf(fp,"%s\n",ep->d_name);
            }
            (void) closedir (dp);
        } else perror ("Couldn't open the directory");
        fclose(fp);

        fp = fopen("./characters/characters.txt","r");	
        int i = 0; 
        while(1){ 
            char r = (char)fgetc(fp); 
            int k = 0; 
            while(r!='\n' && !feof(fp))
            {
                characters[i][k++] = r;			
                r = (char)fgetc(fp); 
            } 
            characters[i][k]=0;		
            if(feof(fp))
            {		 
                break; 
            } 
            i++; 
        } 
        fclose(fp);
        fp = fopen("./weapons/weapons.txt","r");		
        i = 0; 
        while(1){ 
            char r = (char)fgetc(fp); 
            int k = 0; 
            while(r!='\n' && !feof(fp))
            {	
                weapons[i][k++] = r;			
                r = (char)fgetc(fp); 
            } 
            weapons[i][k]=0;		
            if(feof(fp))
            {		
                break; 
            } 
            i++; 
        } 
        fclose(fp);

        srand(time(0));
        while(1)
        {
            jmlgacha++;
            if(jmlgacha&1)
            {
                strcpy(tipeItem,"characters");
                strcpy(namaItem,"./");
                strcat(namaItem,tipeItem);
                strcat(namaItem,"/");
                strcat(namaItem,characters[rand()%48]);
                fp = fopen(namaItem,"r");
                fread(buffer, 10240, 1, fp);
                fclose(fp);

                parsed_json = json_tokener_parse(buffer);
            }else
            {
                strcpy(tipeItem,"weapons");
                strcpy(namaItem,"./");
                strcat(namaItem,tipeItem);
                strcat(namaItem,"/");
                strcat(namaItem,weapons[rand()%130]);
                fp = fopen(namaItem,"r");
                fread(buffer, 10240, 1, fp);
                fclose(fp);

                parsed_json = json_tokener_parse(buffer);
            }

            if(jmlgacha%10==0)
            {
                // Kode untuk membuat File baru dan menyimpan hasil gacha
            }
            if(jmlgacha%90==0)
            {
                // Kode untuk membuat Folder baru dan menyimpan file gacha
            }
        }
    }
}
```

## **Membuat Format Penamaan File dan Folder** ##
Pada bagian c, kita diberikan aturan terkait bagaimana cara menuliskan format penamaan file dan folder yang dibuat di dalam working directory kita. 
### Format Nama File ###
Berikut adalah program untuk mengatur nama file yang kita buat berdasarkan {Hh:Mm:Ss}\_gacha\_{jumlah-gacha}. Kemudian kita menyimpan seluruh nama file yang kita buat ke dalam sebuah array hasilNamaFile agar nanti bisa kita gunakan pada saat ingin memindahkan file kita ke dalam folder.
```c
sleep(1);
snprintf(namaFile,sizeof(namaFile),"./gacha-gacha/%.2d:%.2d:%.2d_gacha_%d.txt",Sys_T->tm_hour,Sys_T->tm_min,Sys_T->tm_sec,jmlgacha);
strcpy(hasilNamaFile[(jmlgacha/10)%9],namaFile);

fp=fopen(namaFile,"w");
strcpy(path,"./gacha-gacha/");
dp = opendir(path);
if (dp != NULL)
{
    n=0;
    while (n<10) {
        fprintf(fp,"%s",hasil_gacha[n++]);
    }
    (void) closedir (dp);
} else perror ("Couldn't open the directory");
fclose(fp);
```
Karena perbedaan waktu pembuatan file adalah satu detik sehingga kita menambahkan fungsi sleep(1)
### Format Nama Folder ###
Berikut adalah program untuk mengatur nama folder yang kita buat berdasarkan total\_gacha\_{jumlah-gacha}. Karena untuk membuat folder kita menggunakan exec, maka kita akan membuat fork dan wait di dalamnya agar proses programnya tidak berhenti setelah foldernya dibuat, dan menambahkan wait() untuk menunggu child prosesnya selesai dibuat.
```c
snprintf(namaFolder,sizeof(namaFolder),"./gacha-gacha/total_gacha_%d/",jmlgacha);
int status;
pid_t child_id;
child_id = fork();
if(child_id < 0) exit(EXIT_FAILURE);
else if (child_id == 0)
{
    char *argv[] = {"mkdir","-p", namaFolder,NULL};
    execv("/usr/bin/mkdir", argv);
}
else 
{
    while((wait(&status))>0);
    // Program untuk memindahkan File kedalam Folder
}
```
### Memindahkan File-file Gacha ke Dalam Folder ###
Setelah Format nama file dan foldernya selesai dibuat, maka selanjutnya kita akan menyelesaikan program pada poin b yaitu memindahkan file ke dalam folder.
```c
int p=0;
while(p<9)
{
    int status;
    pid_t child_id;
    child_id = fork();
    if(child_id < 0) exit(EXIT_FAILURE);
    else if (child_id == 0)
    {
        char *argv[] = {"mv",hasilNamaFile[p], namaFolder,NULL};
        execv("/usr/bin/mv", argv);
    }
    else 
    {
        while((wait(&status))>0);
        p++;
    }
}
```

## **Menggabung Seluruh Kode Hingga Saat ini** ##
Setelah seluruh perintah dari soal1 bagian b terpenuhi, kita akan menggabungkan keseluruhan kode dari bagian a dan b.
```c
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<stdio.h>
#include<wait.h>
#include<string.h>
#include<pwd.h>
#include<sys/stat.h>
#include<dirent.h>
#include<json-c/json.h>
#include<time.h>

int jmlgacha=0;

char characters[48][100];
char weapons[130][100];

int main(int argc, char **argv)
{
    int status;
    pid_t child_id;
    child_id = fork();
    if(child_id < 0) exit(EXIT_FAILURE);
    else if (child_id == 0)
    {
        int status;
        pid_t child_id;
        child_id = fork();
        if(child_id < 0) exit(EXIT_FAILURE);
        else if (child_id == 0)
        {
            int status;
            pid_t child_id;
            child_id = fork();
            if(child_id < 0) exit(EXIT_FAILURE);
            else if (child_id == 0)
            {
                int status;
                pid_t child_id;
                child_id = fork();
                if(child_id < 0) exit(EXIT_FAILURE);
                else if (child_id == 0)
                {
                    int status;
                    pid_t child_id;
                    child_id = fork();
                    if(child_id < 0) exit(EXIT_FAILURE);
                    else if (child_id == 0)
                    {
                        int status;
                        pid_t child_id;
                        child_id = fork();
                        if(child_id < 0) exit(EXIT_FAILURE);
                        else if (child_id == 0)
                        {
                            char *argv[] = {"wget","-O","./file1.zip","https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp","-qq",NULL};
                            execv("/usr/bin/wget", argv);
                        }
                        else
                        {
                            while((wait(&status))>0);
                            char *argv[] = {"wget","-O","./file2.zip","https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT","-qq",NULL};
                            execv("/usr/bin/wget", argv);
                        }
                    }
                    else
                    {
                        while((wait(&status))>0);
                        char *argv[] = {"unzip","-qq","file1.zip", NULL};
                        execv("/bin/unzip", argv);
                    }
                }
                else
                {
                    while((wait(&status))>0);
                    char *argv[] = {"unzip","-qq","file2.zip", NULL};
                    execv("/bin/unzip", argv);
                }
            }
            else 
            {
                while((wait(&status))>0);
                char *argv[] = {"mkdir","-p", "./gacha-gacha", NULL};
                execv("/usr/bin/mkdir", argv);
            }
        }
        else 
        {
            while((wait(&status))>0);
            char *argv[] = {"rm", "./file1.zip", "./file2.zip", NULL};
            execv("/usr/bin/rm", argv);
        }
    }
    else 
    {
        while((wait(&status))>0);
        DIR *dp;
                
        struct dirent *ep;
        char path[100];
        int n=0,m=0;
        srand(time(0));

        FILE *fp;
        fp=fopen("./characters/characters.txt","w");
        strcpy(path,"./characters");
        dp = opendir(path);
        if (dp != NULL)
        {
            while ((ep = readdir (dp))) {
                if(strlen(ep->d_name)>2) fprintf(fp,"%s\n",ep->d_name);
            }
            (void) closedir (dp);
        } else perror ("Couldn't open the directory");
        fclose(fp);

        fp=fopen("./weapons/weapons.txt","w");
        strcpy(path,"./weapons");
        dp = opendir(path);
        if (dp != NULL)
        {
            while ((ep = readdir (dp))) {
                if(strlen(ep->d_name)>2) fprintf(fp,"%s\n",ep->d_name);
            }
            (void) closedir (dp);
        } else perror ("Couldn't open the directory");
        fclose(fp);

        fp = fopen("./characters/characters.txt","r");	
        int i = 0; 
        while(1){ 
            char r = (char)fgetc(fp); 
            int k = 0; 
            while(r!='\n' && !feof(fp))
            {
                characters[i][k++] = r;			
                r = (char)fgetc(fp); 
            } 
            characters[i][k]=0;		
            if(feof(fp))
            {		 
                break; 
            } 
            i++; 
        } 
        fclose(fp);
        fp = fopen("./weapons/weapons.txt","r");		
        i = 0; 
        while(1){ 
            char r = (char)fgetc(fp); 
            int k = 0; 
            while(r!='\n' && !feof(fp))
            {	
                weapons[i][k++] = r;			
                r = (char)fgetc(fp); 
            } 
            weapons[i][k]=0;		
            if(feof(fp))
            {		
                break; 
            } 
            i++; 
        } 
        fclose(fp);

        srand(time(0));
        while(1)
        {
            jmlgacha++;
            if(jmlgacha&1)
            {
                strcpy(tipeItem,"characters");
                strcpy(namaItem,"./");
                strcat(namaItem,tipeItem);
                strcat(namaItem,"/");
                strcat(namaItem,characters[rand()%48]);
                fp = fopen(namaItem,"r");
                fread(buffer, 10240, 1, fp);
                fclose(fp);

                parsed_json = json_tokener_parse(buffer);
            }else
            {
                strcpy(tipeItem,"weapons");
                strcpy(namaItem,"./");
                strcat(namaItem,tipeItem);
                strcat(namaItem,"/");
                strcat(namaItem,weapons[rand()%130]);
                fp = fopen(namaItem,"r");
                fread(buffer, 10240, 1, fp);
                fclose(fp);

                parsed_json = json_tokener_parse(buffer);
            }

            if(jmlgacha%10==0)
            {
                sleep(1);
                snprintf(namaFile,sizeof(namaFile),"./gacha-gacha/%.2d:%.2d:%.2d_gacha_%d.txt",Sys_T->tm_hour,Sys_T->tm_min,Sys_T->tm_sec,jmlgacha);
                strcpy(hasilNamaFile[(jmlgacha/10)%9],namaFile);

                fp=fopen(namaFile,"w");
                strcpy(path,"./gacha-gacha/");
                dp = opendir(path);
                if (dp != NULL)
                {
                    n=0;
                    while (n<10) {
                        fprintf(fp,"%s",hasil_gacha[n++]);
                    }
                    (void) closedir (dp);
                } else perror ("Couldn't open the directory");
                fclose(fp);
            }
            if(jmlgacha%90==0)
            {
                int status;
                pid_t child_id;
                child_id = fork();
                if(child_id < 0) exit(EXIT_FAILURE);
                else if (child_id == 0)
                {
                    char *argv[] = {"mkdir","-p", namaFolder,NULL};
                    execv("/usr/bin/mkdir", argv);
                }
                else 
                {
                    while((wait(&status))>0);
                    int p=0;
                    while(p<9)
                    {
                        int status;
                        pid_t child_id;
                        child_id = fork();
                        if(child_id < 0) exit(EXIT_FAILURE);
                        else if (child_id == 0)
                        {
                            char *argv[] = {"mv",hasilNamaFile[p], namaFolder,NULL};
                            execv("/usr/bin/mv", argv);
                        }
                        else 
                        {
                            while((wait(&status))>0);
                            p++;
                        }
                    }
                }
            }
        }
    }
}
```
## **Membuat Format Penamaan File dan Folder** ##
Pada bagian d, dijelaskan tentang aturan gachanya terkait primogems dan isi dari gachanya. 
### Mengambil atribut dari File Json ###
Pada saat melakukan gacha, sebuah file item berbentuk JSON akan diambil secara random. Di dalam file JSON tersebut akan diambil dua atribut yaitu name dan rarity untuk dimasukkan kedalam hasil gacha
```c
json_object_object_get_ex(parsed_json, "name", &name);
json_object_object_get_ex(parsed_json, "rarity", &rarity);
```
### Format Nama Hasil Gacha ###
Setelah kedua atribut tersebut diambil, maka akan dibuat hasil gacha dan dimasukkan kedalam variable array hasil_gacha dengan format {jumlah-gacha}\_[tipe-item]\_{rarity}\_{name}\_{sisa-primogems}
```c
snprintf(hasil_gacha[(jmlgacha-1)%10],sizeof(hasil_gacha[(jmlgacha-1)%10]),"%d_%s_%s_%s_%d\n",jmlgacha,tipeItem,json_object_get_string(rarity),json_object_get_string(name),primogems);
```
### Primogems ###
Karena proses gacha memerlukan penggunaan primogems sebanyak 160 dan dengan modal awal primogems adalah 79000, maka kodenya dapat dimodifikasi dengan menambahkan pengurangan primogems. Sehingga Secara Keseluruhan Kodenya akan berubah menjadi seperti berikut
```c
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<stdio.h>
#include<wait.h>
#include<string.h>
#include<pwd.h>
#include<sys/stat.h>
#include<dirent.h>
#include<json-c/json.h>
#include<time.h>

int jmlgacha=0;
int primogems=79000;

char characters[48][100];
char weapons[130][100];

int main(int argc, char **argv)
{
    int status;
    pid_t child_id;
    child_id = fork();
    if(child_id < 0) exit(EXIT_FAILURE);
    else if (child_id == 0)
    {
        int status;
        pid_t child_id;
        child_id = fork();
        if(child_id < 0) exit(EXIT_FAILURE);
        else if (child_id == 0)
        {
            int status;
            pid_t child_id;
            child_id = fork();
            if(child_id < 0) exit(EXIT_FAILURE);
            else if (child_id == 0)
            {
                int status;
                pid_t child_id;
                child_id = fork();
                if(child_id < 0) exit(EXIT_FAILURE);
                else if (child_id == 0)
                {
                    int status;
                    pid_t child_id;
                    child_id = fork();
                    if(child_id < 0) exit(EXIT_FAILURE);
                    else if (child_id == 0)
                    {
                        int status;
                        pid_t child_id;
                        child_id = fork();
                        if(child_id < 0) exit(EXIT_FAILURE);
                        else if (child_id == 0)
                        {
                            char *argv[] = {"wget","-O","./file1.zip","https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp","-qq",NULL};
                            execv("/usr/bin/wget", argv);
                        }
                        else
                        {
                            while((wait(&status))>0);
                            char *argv[] = {"wget","-O","./file2.zip","https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT","-qq",NULL};
                            execv("/usr/bin/wget", argv);
                        }
                    }
                    else
                    {
                        while((wait(&status))>0);
                        char *argv[] = {"unzip","-qq","file1.zip", NULL};
                        execv("/bin/unzip", argv);
                    }
                }
                else
                {
                    while((wait(&status))>0);
                    char *argv[] = {"unzip","-qq","file2.zip", NULL};
                    execv("/bin/unzip", argv);
                }
            }
            else 
            {
                while((wait(&status))>0);
                char *argv[] = {"mkdir","-p", "./gacha-gacha", NULL};
                execv("/usr/bin/mkdir", argv);
            }
        }
        else 
        {
            while((wait(&status))>0);
            char *argv[] = {"rm", "./file1.zip", "./file2.zip", NULL};
            execv("/usr/bin/rm", argv);
        }
    }
    else 
    {
        while((wait(&status))>0);
        DIR *dp;
                
        struct dirent *ep;
        char path[100];
        int n=0,m=0;
        srand(time(0));

        FILE *fp;
        fp=fopen("./characters/characters.txt","w");
        strcpy(path,"./characters");
        dp = opendir(path);
        if (dp != NULL)
        {
            while ((ep = readdir (dp))) {
                if(strlen(ep->d_name)>2) fprintf(fp,"%s\n",ep->d_name);
            }
            (void) closedir (dp);
        } else perror ("Couldn't open the directory");
        fclose(fp);

        fp=fopen("./weapons/weapons.txt","w");
        strcpy(path,"./weapons");
        dp = opendir(path);
        if (dp != NULL)
        {
            while ((ep = readdir (dp))) {
                if(strlen(ep->d_name)>2) fprintf(fp,"%s\n",ep->d_name);
            }
            (void) closedir (dp);
        } else perror ("Couldn't open the directory");
        fclose(fp);

        fp = fopen("./characters/characters.txt","r");	
        int i = 0; 
        while(1){ 
            char r = (char)fgetc(fp); 
            int k = 0; 
            while(r!='\n' && !feof(fp))
            {
                characters[i][k++] = r;			
                r = (char)fgetc(fp); 
            } 
            characters[i][k]=0;		
            if(feof(fp))
            {		 
                break; 
            } 
            i++; 
        } 
        fclose(fp);
        fp = fopen("./weapons/weapons.txt","r");		
        i = 0; 
        while(1){ 
            char r = (char)fgetc(fp); 
            int k = 0; 
            while(r!='\n' && !feof(fp))
            {	
                weapons[i][k++] = r;			
                r = (char)fgetc(fp); 
            } 
            weapons[i][k]=0;		
            if(feof(fp))
            {		
                break; 
            } 
            i++; 
        } 
        fclose(fp);

        srand(time(0));
        while(primogems>=160)
        {
            jmlgacha++;
            primogems-=160;
            char* buffer;
            buffer = (char*)calloc(1024000,sizeof(char));
            struct json_object *parsed_json;
            struct json_object *name;
            struct json_object *rarity;
            char tipeItem[20]; 
            char namaItem[30];
            if(jmlgacha&1)
            {
                strcpy(tipeItem,"characters");
                strcpy(namaItem,"./");
                strcat(namaItem,tipeItem);
                strcat(namaItem,"/");
                strcat(namaItem,characters[rand()%48]);
                fp = fopen(namaItem,"r");
                fread(buffer, 10240, 1, fp);
                fclose(fp);

                parsed_json = json_tokener_parse(buffer);
            }else
            {
                strcpy(tipeItem,"weapons");
                strcpy(namaItem,"./");
                strcat(namaItem,tipeItem);
                strcat(namaItem,"/");
                strcat(namaItem,weapons[rand()%130]);
                fp = fopen(namaItem,"r");
                fread(buffer, 10240, 1, fp);
                fclose(fp);

                parsed_json = json_tokener_parse(buffer);
            }

            if(jmlgacha%10==0)
            {
                sleep(1);
                snprintf(namaFile,sizeof(namaFile),"./gacha-gacha/%.2d:%.2d:%.2d_gacha_%d.txt",Sys_T->tm_hour,Sys_T->tm_min,Sys_T->tm_sec,jmlgacha);
                strcpy(hasilNamaFile[(jmlgacha/10)%9],namaFile);

                fp=fopen(namaFile,"w");
                strcpy(path,"./gacha-gacha/");
                dp = opendir(path);
                if (dp != NULL)
                {
                    n=0;
                    while (n<10) {
                        fprintf(fp,"%s",hasil_gacha[n++]);
                    }
                    (void) closedir (dp);
                } else perror ("Couldn't open the directory");
                fclose(fp);
            }
            if(jmlgacha%90==0)
            {
                int status;
                pid_t child_id;
                child_id = fork();
                if(child_id < 0) exit(EXIT_FAILURE);
                else if (child_id == 0)
                {
                    char *argv[] = {"mkdir","-p", namaFolder,NULL};
                    execv("/usr/bin/mkdir", argv);
                }
                else 
                {
                    while((wait(&status))>0);
                    int p=0;
                    while(p<9)
                    {
                        int status;
                        pid_t child_id;
                        child_id = fork();
                        if(child_id < 0) exit(EXIT_FAILURE);
                        else if (child_id == 0)
                        {
                            char *argv[] = {"mv",hasilNamaFile[p], namaFolder,NULL};
                            execv("/usr/bin/mv", argv);
                        }
                        else 
                        {
                            while((wait(&status))>0);
                            p++;
                        }
                    }
                }
            }
        }
    }
}
```
## **Menjalankan Program Pada Waktu Tertentu** ##
Pada bagian e, program kita diharapkan berjalan pada 30 Maret jam 04:44, dan 3 jam kemudian akan melakukan zip terhadap folder gacha-gacha dan menghapus file lain yang tersisa. 
### Menjalankan Program Kita Pada tanggal 30 Maret jam 04:44 ###
Berikut adalah kode untuk menjalankan program utama kita pada tanggal 30 maret jam 04:44
```c
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<stdio.h>
#include<wait.h>
#include<string.h>
#include<pwd.h>
#include<sys/stat.h>
#include<dirent.h>
#include<json-c/json.h>
#include<time.h>

int jmlgacha=0;
int primogems=79000;

char characters[48][100];
char weapons[130][100];

int main(int argc, char **argv)
{
    while (1) {
        struct tm *Sys_T2;

        time_t Tval2;
        Tval2 = time(NULL);
        Sys_T2 = localtime(&Tval2);
        char startTime[] = "3-30 04:44\n";
        char nowTime[30];
        snprintf(nowTime,sizeof(nowTime),"%d-%d %.2d:%.2d\n", (Sys_T2->tm_mon)+1,Sys_T2->tm_mday,Sys_T2->tm_hour,Sys_T2->tm_min);
        if(strcmp(nowTime,startTime)==0)
        {
            // Program Utama Kita yaitu kodingan yang berada dalam main()
        }
        sleep(10);
    }
}
```
sleep(10) akan membuat program kita akan menunggu setiap 10 detik kemudian mengecek waktunya kembali.
### Melakukan Zip pada Folder Gacha-gacha ###
Berikut adalah kode untuk melakukan zip terhadap folder gacha-gacha dengan format nama not\_safe\_for\_wibu yang disertai password "satuduatiga".
```c
char *argv[] = {"zip","-r","-e","-P","satuduatiga","not_safe_for_wibu","gacha-gacha", "-q", NULL};
execv("/usr/bin/zip", argv);
```
### Mengatur waktu Zip Program Kita Pada tanggal 30 Maret jam 07:44 ###
Berikut adalah kode untuk mengatur agar proses zip terhadap folder gacha-gacha dilakukan pada tanggal 30 maret jam 07:44
```c
while(1)
{
    Tval2 = time(NULL);
    Sys_T2 = localtime(&Tval2);
    snprintf(nowTime,sizeof(nowTime),"%d-%d %.2d:%.2d\n", (Sys_T2->tm_mon)+1,Sys_T2->tm_mday,Sys_T2->tm_hour,Sys_T2->tm_min);
    if(strcmp(nowTime,endTime)==0)
    {
        char *argv[] = {"zip","-r","-e","-P","satuduatiga","not_safe_for_wibu","gacha-gacha", "-q", NULL};
        execv("/usr/bin/zip", argv);
    }
    sleep(10);
}
```
### Menghapus Folder yang Tersisa Setelah Proses Zip ###
Berikut adalah kode untuk menghapus folder yang tersisa setelah proses zip dilakukan.
```c
char *argv[] = {"rm", "-r", "gacha-gacha","characters","weapons", NULL};
execv("/usr/bin/rm", argv);
```
## **Menggabungkan Seluruh Kodingan Hingga Saat ini** ##
Setelah poin e rampung, berikut adalah tampak dari kodingan kita hingga saat ini.
```c
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<stdio.h>
#include<wait.h>
#include<string.h>
#include<pwd.h>
#include<sys/stat.h>
#include<dirent.h>
#include<json-c/json.h>
#include<time.h>

char user[100];
int jmlgacha = 0;
int primogems = 79000;
char characters[48][100];
char weapons[130][100];

int main(int argc, char **argv)
{
    while (1) {
        struct tm *Sys_T2;

        time_t Tval2;
        Tval2 = time(NULL);
        Sys_T2 = localtime(&Tval2);
        char startTime[] = "3-30 04:44\n";
        char nowTime[30];
        snprintf(nowTime,sizeof(nowTime),"%d-%d %.2d:%.2d\n", (Sys_T2->tm_mon)+1,Sys_T2->tm_mday,Sys_T2->tm_hour,Sys_T2->tm_min);
        if(strcmp(nowTime,startTime)==0)
        {
            int status;
            pid_t child_id;
            child_id = fork();
            if(child_id < 0) exit(EXIT_FAILURE);
            else if (child_id == 0)
            {
                int status;
                pid_t child_id;
                child_id = fork();
                if(child_id < 0) exit(EXIT_FAILURE);
                else if (child_id == 0)
                {
                    int status;
                    pid_t child_id;
                    child_id = fork();
                    if(child_id < 0) exit(EXIT_FAILURE);
                    else if (child_id == 0)
                    {
                        int status;
                        pid_t child_id;
                        child_id = fork();
                        if(child_id < 0) exit(EXIT_FAILURE);
                        else if (child_id == 0)
                        {
                            int status;
                            pid_t child_id;
                            child_id = fork();
                            if(child_id < 0) exit(EXIT_FAILURE);
                            else if (child_id == 0)
                            {
                                int status;
                                pid_t child_id;
                                child_id = fork();
                                if(child_id < 0) exit(EXIT_FAILURE);
                                else if (child_id == 0)
                                {
                                    int status;
                                    pid_t child_id;
                                    child_id = fork();
                                    if(child_id < 0) exit(EXIT_FAILURE);
                                    else if (child_id == 0)
                                    {
                                        char *argv[] = {"wget","-O","./file1.zip","https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp","-qq",NULL};
                                        execv("/usr/bin/wget", argv);
                                    }
                                    else
                                    {
                                        while((wait(&status))>0);
                                        char *argv[] = {"wget","-O","./file2.zip","https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT","-qq",NULL};
                                        execv("/usr/bin/wget", argv);
                                    }
                                }
                                else
                                {
                                    while((wait(&status))>0);
                                    char *argv[] = {"unzip","-qq","file1.zip", NULL};
                                    execv("/bin/unzip", argv);
                                }
                            }
                            else
                            {
                                while((wait(&status))>0);
                                char *argv[] = {"unzip","-qq","file2.zip", NULL};
                                execv("/bin/unzip", argv);
                            }
                        }
                        else 
                        {
                            while((wait(&status))>0);
                            char *argv[] = {"mkdir","-p", "./gacha-gacha", NULL};
                            execv("/usr/bin/mkdir", argv);
                        }
                    }
                    else 
                    {
                        while((wait(&status))>0);
                        char *argv[] = {"rm", "./file1.zip", "./file2.zip", NULL};
                        execv("/usr/bin/rm", argv);
                    }
                }
                else 
                {
                    while((wait(&status))>0);
                    DIR *dp;
                
                    struct dirent *ep;
                    char path[100];
                    int n=0,m=0;
                    srand(time(0));

                    FILE *fp;
                    fp=fopen("./characters/characters.txt","w");
                    strcpy(path,"./characters");
                    dp = opendir(path);
                    if (dp != NULL)
                    {
                        while ((ep = readdir (dp))) {
                            if(strlen(ep->d_name)>2) fprintf(fp,"%s\n",ep->d_name);
                        }
                        (void) closedir (dp);
                    } else perror ("Couldn't open the directory");
                    fclose(fp);

                    fp=fopen("./weapons/weapons.txt","w");
                    strcpy(path,"./weapons");
                    dp = opendir(path);
                    if (dp != NULL)
                    {
                        while ((ep = readdir (dp))) {
                            if(strlen(ep->d_name)>2) fprintf(fp,"%s\n",ep->d_name);
                        }
                        (void) closedir (dp);
                    } else perror ("Couldn't open the directory");
                    fclose(fp);

                    fp = fopen("./characters/characters.txt","r");	
                    int i = 0; 
                    while(1){ 
                        char r = (char)fgetc(fp); 
                        int k = 0; 
                        while(r!='\n' && !feof(fp))
                        {
                            characters[i][k++] = r;			
                            r = (char)fgetc(fp); 
                        } 
                        characters[i][k]=0;		
                        if(feof(fp))
                        {		 
                            break; 
                        } 
                        i++; 
                    } 
                    fclose(fp);
                    fp = fopen("./weapons/weapons.txt","r");		
                    i = 0; 
                    while(1){ 
                        char r = (char)fgetc(fp); 
                        int k = 0; 
                        while(r!='\n' && !feof(fp))
                        {	
                            weapons[i][k++] = r;			
                            r = (char)fgetc(fp); 
                        } 
                        weapons[i][k]=0;		
                        if(feof(fp))
                        {		
                            break; 
                        } 
                        i++; 
                    } 
                    fclose(fp);

                    char hasil_gacha[10][200];
                    char temp[50];
                    char namaFile[40];
                    char hasilNamaFile[100][100];
                    char namaFolder[40];
                    

                    while(primogems>=160)
                    {
                        struct tm *Sys_T;

                        time_t Tval;
                        Tval = time(NULL);
                        Sys_T = localtime(&Tval);

                        char* buffer;
                        buffer = (char*)calloc(1024000,sizeof(char));
                        struct json_object *parsed_json;
                        struct json_object *name;
                        struct json_object *rarity;
                        char tipeItem[20]; 
                        char namaItem[30];
                        jmlgacha++;
                        primogems-=160;

                        if(jmlgacha&1)
                        {
                            strcpy(tipeItem,"characters");
                            strcpy(namaItem,"./");
                            strcat(namaItem,tipeItem);
                            strcat(namaItem,"/");
                            strcat(namaItem,characters[rand()%48]);
                            fp = fopen(namaItem,"r");
                            fread(buffer, 10240, 1, fp);
                            fclose(fp);

                            parsed_json = json_tokener_parse(buffer);

                            json_object_object_get_ex(parsed_json, "name", &name);
                            json_object_object_get_ex(parsed_json, "rarity", &rarity);

                            snprintf(hasil_gacha[(jmlgacha-1)%10],sizeof(hasil_gacha[(jmlgacha-1)%10]),"%d_%s_%s_%s_%d\n",jmlgacha,tipeItem,json_object_get_string(rarity),json_object_get_string(name),primogems);
                        }else
                        {
                            strcpy(tipeItem,"weapons");
                            strcpy(namaItem,"./");
                            strcat(namaItem,tipeItem);
                            strcat(namaItem,"/");
                            strcat(namaItem,weapons[rand()%130]);
                            fp = fopen(namaItem,"r");
                            fread(buffer, 10240, 1, fp);
                            fclose(fp);

                            parsed_json = json_tokener_parse(buffer);

                            json_object_object_get_ex(parsed_json, "name", &name);
                            json_object_object_get_ex(parsed_json, "rarity", &rarity);
                            
                            snprintf(hasil_gacha[(jmlgacha-1)%10],sizeof(hasil_gacha[(jmlgacha-1)%10]),"%d_%s_%s_%s_%d\n",jmlgacha,tipeItem,json_object_get_string(rarity),json_object_get_string(name),primogems);
                        }
                        
                        free(buffer);
                        if(jmlgacha%10==0)
                        {
                            sleep(1);
                            snprintf(namaFile,sizeof(namaFile),"./gacha-gacha/%.2d:%.2d:%.2d_gacha_%d.txt",Sys_T->tm_hour,Sys_T->tm_min,Sys_T->tm_sec,jmlgacha);
                            strcpy(hasilNamaFile[(jmlgacha/10)%9],namaFile);
                            

                            fp=fopen(namaFile,"w");
                            strcpy(path,"./gacha-gacha/");
                            dp = opendir(path);
                            if (dp != NULL)
                            {
                                n=0;
                                while (n<10) {
                                    fprintf(fp,"%s",hasil_gacha[n++]);
                                }
                                (void) closedir (dp);
                            } else perror ("Couldn't open the directory");
                            fclose(fp);
                        }
                        if(jmlgacha%90==0)
                        {
                            snprintf(namaFolder,sizeof(namaFolder),"./gacha-gacha/total_gacha_%d/",jmlgacha);
                            int status;
                            pid_t child_id;
                            child_id = fork();
                            if(child_id < 0) exit(EXIT_FAILURE);
                            else if (child_id == 0)
                            {
                                char *argv[] = {"mkdir","-p", namaFolder,NULL};
                                execv("/usr/bin/mkdir", argv);
                            }
                            else 
                            {
                                while((wait(&status))>0);
                                int p=0;
                                while(p<9)
                                {
                                    int status;
                                    pid_t child_id;
                                    child_id = fork();
                                    if(child_id < 0) exit(EXIT_FAILURE);
                                    else if (child_id == 0)
                                    {
                                        char *argv[] = {"mv",hasilNamaFile[p], namaFolder,NULL};
                                        execv("/usr/bin/mv", argv);
                                    }
                                    else 
                                    {
                                        while((wait(&status))>0);
                                        p++;
                                    }
                                }
                            }
                        }
                    }
                    char endTime[] = "3-30 07:44\n";
                        
                    while(1)
                    {
                        Tval2 = time(NULL);
                        Sys_T2 = localtime(&Tval2);
                        snprintf(nowTime,sizeof(nowTime),"%d-%d %.2d:%.2d\n", (Sys_T2->tm_mon)+1,Sys_T2->tm_mday,Sys_T2->tm_hour,Sys_T2->tm_min);
                        if(strcmp(nowTime,endTime)==0)
                        {
                            char *argv[] = {"zip","-r","-e","-P","satuduatiga","not_safe_for_wibu","gacha-gacha", "-q", NULL};
                            execv("/usr/bin/zip", argv);
                        }
                        sleep(10);
                    }
                }
            }
            else 
            {
                while((wait(&status))>0);
                char *argv[] = {"rm", "-r", "gacha-gacha","characters","weapons", NULL};
                execv("/usr/bin/rm", argv);
            }

        }
        sleep(10);
    }
}
```
## **Menjalankan Dalam Latar Belakang** ##
Pada bagian Note poin 4, programnya diarakahkan agar dapat berjalan di latar belakang, dalam artian dia akan mengubah prosesnya kebentuk daemon procces.
Beriku adalah Bentuk Kodingan akhir setelah diubah menjadi daemon procces
```c
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<stdio.h>
#include<wait.h>
#include<string.h>
#include<pwd.h>
#include<sys/stat.h>
#include<dirent.h>
#include<json-c/json.h>
#include<time.h>

char user[100];
int jmlgacha = 0;
int primogems = 79000;
char characters[48][100];
char weapons[130][100];

int main(int argc, char **argv)
{
    strcpy(user, getenv("USER"));

    pid_t pid, sid; 

    pid = fork();   

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    char pathProgram[20]="/home/";
    strcat(pathProgram,user);
    strcat(pathProgram,"/Sisop/Shift/soal-shift-sisop-modul-2-E11-2022/soal1/");
    if ((chdir(pathProgram)) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1) {
        struct tm *Sys_T2;

        time_t Tval2;
        Tval2 = time(NULL);
        Sys_T2 = localtime(&Tval2);
        char startTime[] = "3-30 04:44\n";
        char nowTime[30];
        snprintf(nowTime,sizeof(nowTime),"%d-%d %.2d:%.2d\n", (Sys_T2->tm_mon)+1,Sys_T2->tm_mday,Sys_T2->tm_hour,Sys_T2->tm_min);
        if(strcmp(nowTime,startTime)==0)
        {
            int status;
            pid_t child_id;
            child_id = fork();
            if(child_id < 0) exit(EXIT_FAILURE);
            else if (child_id == 0)
            {
                int status;
                pid_t child_id;
                child_id = fork();
                if(child_id < 0) exit(EXIT_FAILURE);
                else if (child_id == 0)
                {
                    int status;
                    pid_t child_id;
                    child_id = fork();
                    if(child_id < 0) exit(EXIT_FAILURE);
                    else if (child_id == 0)
                    {
                        int status;
                        pid_t child_id;
                        child_id = fork();
                        if(child_id < 0) exit(EXIT_FAILURE);
                        else if (child_id == 0)
                        {
                            int status;
                            pid_t child_id;
                            child_id = fork();
                            if(child_id < 0) exit(EXIT_FAILURE);
                            else if (child_id == 0)
                            {
                                int status;
                                pid_t child_id;
                                child_id = fork();
                                if(child_id < 0) exit(EXIT_FAILURE);
                                else if (child_id == 0)
                                {
                                    int status;
                                    pid_t child_id;
                                    child_id = fork();
                                    if(child_id < 0) exit(EXIT_FAILURE);
                                    else if (child_id == 0)
                                    {
                                        char *argv[] = {"wget","-O","./file1.zip","https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp","-qq",NULL};
                                        execv("/usr/bin/wget", argv);
                                    }
                                    else
                                    {
                                        while((wait(&status))>0);
                                        char *argv[] = {"wget","-O","./file2.zip","https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT","-qq",NULL};
                                        execv("/usr/bin/wget", argv);
                                    }
                                }
                                else
                                {
                                    while((wait(&status))>0);
                                    char *argv[] = {"unzip","-qq","file1.zip", NULL};
                                    execv("/bin/unzip", argv);
                                }
                            }
                            else
                            {
                                while((wait(&status))>0);
                                char *argv[] = {"unzip","-qq","file2.zip", NULL};
                                execv("/bin/unzip", argv);
                            }
                        }
                        else 
                        {
                            while((wait(&status))>0);
                            char *argv[] = {"mkdir","-p", "./gacha-gacha", NULL};
                            execv("/usr/bin/mkdir", argv);
                        }
                    }
                    else 
                    {
                        while((wait(&status))>0);
                        char *argv[] = {"rm", "./file1.zip", "./file2.zip", NULL};
                        execv("/usr/bin/rm", argv);
                    }
                }
                else 
                {
                    while((wait(&status))>0);
                    DIR *dp;
                
                    struct dirent *ep;
                    char path[100];
                    int n=0,m=0;
                    srand(time(0));

                    FILE *fp;
                    fp=fopen("./characters/characters.txt","w");
                    strcpy(path,"./characters");
                    dp = opendir(path);
                    if (dp != NULL)
                    {
                        while ((ep = readdir (dp))) {
                            if(strlen(ep->d_name)>2) fprintf(fp,"%s\n",ep->d_name);
                        }
                        (void) closedir (dp);
                    } else perror ("Couldn't open the directory");
                    fclose(fp);

                    fp=fopen("./weapons/weapons.txt","w");
                    strcpy(path,"./weapons");
                    dp = opendir(path);
                    if (dp != NULL)
                    {
                        while ((ep = readdir (dp))) {
                            if(strlen(ep->d_name)>2) fprintf(fp,"%s\n",ep->d_name);
                        }
                        (void) closedir (dp);
                    } else perror ("Couldn't open the directory");
                    fclose(fp);

                    fp = fopen("./characters/characters.txt","r");	
                    int i = 0; 
                    while(1){ 
                        char r = (char)fgetc(fp); 
                        int k = 0; 
                        while(r!='\n' && !feof(fp))
                        {
                            characters[i][k++] = r;			
                            r = (char)fgetc(fp); 
                        } 
                        characters[i][k]=0;		
                        if(feof(fp))
                        {		 
                            break; 
                        } 
                        i++; 
                    } 
                    fclose(fp);
                    fp = fopen("./weapons/weapons.txt","r");		
                    i = 0; 
                    while(1){ 
                        char r = (char)fgetc(fp); 
                        int k = 0; 
                        while(r!='\n' && !feof(fp))
                        {	
                            weapons[i][k++] = r;			
                            r = (char)fgetc(fp); 
                        } 
                        weapons[i][k]=0;		
                        if(feof(fp))
                        {		
                            break; 
                        } 
                        i++; 
                    } 
                    fclose(fp);

                    char hasil_gacha[10][200];
                    char temp[50];
                    char namaFile[40];
                    char hasilNamaFile[100][100];
                    char namaFolder[40];
                    

                    while(primogems>=160)
                    {
                        struct tm *Sys_T;

                        time_t Tval;
                        Tval = time(NULL);
                        Sys_T = localtime(&Tval);

                        char* buffer;
                        buffer = (char*)calloc(1024000,sizeof(char));
                        struct json_object *parsed_json;
                        struct json_object *name;
                        struct json_object *rarity;
                        char tipeItem[20]; 
                        char namaItem[30];
                        jmlgacha++;
                        primogems-=160;

                        if(jmlgacha&1)
                        {
                            strcpy(tipeItem,"characters");
                            strcpy(namaItem,"./");
                            strcat(namaItem,tipeItem);
                            strcat(namaItem,"/");
                            strcat(namaItem,characters[rand()%48]);
                            fp = fopen(namaItem,"r");
                            fread(buffer, 10240, 1, fp);
                            fclose(fp);

                            parsed_json = json_tokener_parse(buffer);

                            json_object_object_get_ex(parsed_json, "name", &name);
                            json_object_object_get_ex(parsed_json, "rarity", &rarity);

                            snprintf(hasil_gacha[(jmlgacha-1)%10],sizeof(hasil_gacha[(jmlgacha-1)%10]),"%d_%s_%s_%s_%d\n",jmlgacha,tipeItem,json_object_get_string(rarity),json_object_get_string(name),primogems);
                        }else
                        {
                            strcpy(tipeItem,"weapons");
                            strcpy(namaItem,"./");
                            strcat(namaItem,tipeItem);
                            strcat(namaItem,"/");
                            strcat(namaItem,weapons[rand()%130]);
                            fp = fopen(namaItem,"r");
                            fread(buffer, 10240, 1, fp);
                            fclose(fp);

                            parsed_json = json_tokener_parse(buffer);

                            json_object_object_get_ex(parsed_json, "name", &name);
                            json_object_object_get_ex(parsed_json, "rarity", &rarity);
                            
                            snprintf(hasil_gacha[(jmlgacha-1)%10],sizeof(hasil_gacha[(jmlgacha-1)%10]),"%d_%s_%s_%s_%d\n",jmlgacha,tipeItem,json_object_get_string(rarity),json_object_get_string(name),primogems);
                        }
                        
                        free(buffer);
                        if(jmlgacha%10==0)
                        {
                            sleep(1);
                            snprintf(namaFile,sizeof(namaFile),"./gacha-gacha/%.2d:%.2d:%.2d_gacha_%d.txt",Sys_T->tm_hour,Sys_T->tm_min,Sys_T->tm_sec,jmlgacha);
                            strcpy(hasilNamaFile[(jmlgacha/10)%9],namaFile);
                            

                            fp=fopen(namaFile,"w");
                            strcpy(path,"./gacha-gacha/");
                            dp = opendir(path);
                            if (dp != NULL)
                            {
                                n=0;
                                while (n<10) {
                                    fprintf(fp,"%s",hasil_gacha[n++]);
                                }
                                (void) closedir (dp);
                            } else perror ("Couldn't open the directory");
                            fclose(fp);
                        }
                        if(jmlgacha%90==0)
                        {
                            snprintf(namaFolder,sizeof(namaFolder),"./gacha-gacha/total_gacha_%d/",jmlgacha);
                            int status;
                            pid_t child_id;
                            child_id = fork();
                            if(child_id < 0) exit(EXIT_FAILURE);
                            else if (child_id == 0)
                            {
                                char *argv[] = {"mkdir","-p", namaFolder,NULL};
                                execv("/usr/bin/mkdir", argv);
                            }
                            else 
                            {
                                while((wait(&status))>0);
                                int p=0;
                                while(p<9)
                                {
                                    int status;
                                    pid_t child_id;
                                    child_id = fork();
                                    if(child_id < 0) exit(EXIT_FAILURE);
                                    else if (child_id == 0)
                                    {
                                        char *argv[] = {"mv",hasilNamaFile[p], namaFolder,NULL};
                                        execv("/usr/bin/mv", argv);
                                    }
                                    else 
                                    {
                                        while((wait(&status))>0);
                                        p++;
                                    }
                                }
                            }
                        }
                    }
                    char endTime[] = "3-30 07:44\n";
                        
                    while(1)
                    {
                        Tval2 = time(NULL);
                        Sys_T2 = localtime(&Tval2);
                        snprintf(nowTime,sizeof(nowTime),"%d-%d %.2d:%.2d\n", (Sys_T2->tm_mon)+1,Sys_T2->tm_mday,Sys_T2->tm_hour,Sys_T2->tm_min);
                        if(strcmp(nowTime,endTime)==0)
                        {
                            char *argv[] = {"zip","-r","-e","-P","satuduatiga","not_safe_for_wibu","gacha-gacha", "-q", NULL};
                            execv("/usr/bin/zip", argv);
                        }
                        sleep(10);
                    }
                }
            }
            else 
            {
                while((wait(&status))>0);
                char *argv[] = {"rm", "-r", "gacha-gacha","characters","weapons", NULL};
                execv("/usr/bin/rm", argv);
            }

        }
        sleep(10);
    }
}
```
# Soal 2
## **membuat file**
pada soal kita disuruh membuat program untuk membuat folder secara otomatis dimana /home/[user]/shift2/drakor. 
```bash
char path[]="/home/";
strcat(path, user);
strcat(path, "/shift2/drakor");
char *argv[] = {"mkdir", "-p", path, NULL};
execv("/bin/mkdir", argv);
```
kemudian setelah kita memiliki file kita dapat melakukan unzip untuk mendapatkan file .png sesuai yang kita inginkan dengan menambahkan 
 ```"*.png"``` pada ```execl("/bin/unzip","unzip", "drakor.zip", "-d", path, NULL);```
```bash
char path[]="/home/";
strcat(path, user);
strcat(path, "/shift2/drakor");
execl("/bin/unzip","unzip", "drakor.zip", "-d", path, NULL);
```

setelah kita melakukan unzip file kita dapat melakukan fork kembali dimana pada step berikutnya kita dapat menghitung jumbah simbol ; pada nama file untuk membedakan poster tersebut double atau tidak. jika jumlah simbol 2 maka dapat dipastikan poster tersebut satu namun jika jumlah simbol 4 maka dapat dipastikan terdapat dua poster yang menjadi 1. namun jika jumlah simbol 0 itu termmasuk file yang tidak di butuhkan
Banyak request ```$1``` di total dan diabgi dengan banyaknya baris atau ```jam``` dan didapat rata - ratanya
```bash
if(simbol == 0)
{
    //remove folder
    pid_t f;
    f = fork();
    if(f < 0) exit(EXIT_FAILURE);
    else if(f == 0)
    {
        char pathlama[100]="/home/";
        strcat(pathlama, user);
        strcat(pathlama, "/shift2/drakor/");
        strcat(pathlama, name);
        //printf("remove: %s\n", pathlama);
        execl("/bin/rm", "rm", "-r", pathlama, NULL);
    }
    else{}
    }
}

else if(simbol == 2)
{
    int count = 0, print = 1, j = 0;
    char cat[100]= "";
    for(int i = 0; i < strlen(name); i++)
    {
        if(name[i]=='.') print = 0;
        if(count == 2 && print == 1)
        {
            cat[j] = name[i];
            j++;
        }
        if(name[i] == ';') count++;
    }
    pid_t child_id;
    child_id = fork();
    if(child_id < 0) exit(EXIT_FAILURE);
    else if(child_id == 0)
    {
        char path[]="/home/";
        strcat(path, user);
        strcat(path, "/shift2/drakor/");
        strcat(path, cat);
        execlp("/bin/mkdir","mkdir", "-p", path, NULL);
    }
    else
    {
        while((wait(&status))>0);
        pid_t child_ida;
        child_ida=fork();
        if(child_ida<0) exit(EXIT_FAILURE);
        else if(child_ida == 0)
        {
            char pathlama[100]="/home/";
            strcat(pathlama, user);
            strcat(pathlama, "/shift2/drakor/");
            strcat(pathlama, name);
            
            char baru[100]="/home/";
            strcat(baru, user);
            strcat(baru, "/shift2/drakor/");
            strcat(baru, cat);
            strcat(baru, "/");
            strcat(baru, name);
            execlp("/bin/cp","cp", pathlama, baru, NULL);
        }
    else
    {
        while((wait(&status))>0);
        char pathlama[100]="/home/";
        strcat(pathlama, user);
        strcat(pathlama, "/shift2/drakor/");
        strcat(pathlama, name);
        remove(pathlama);
        }
    }
}
```
berikut pengeksekusian jika terdapat 2 poster yang menjadi satu dengan syarat simbol = 4, kurang lebih sama seperti yang program sebelummnya namun untuk memisahkan "_" kita dapat memanggil fungsi strtok() 
```bash
pisah = strtok(NULL, s);
```
setelah kita memisahkan file. langkah selanjutnya kita dapat membuat directory baru sesuai dengan category nya. dengan begitu kita tinggal menyali dengan memanggil program cp 
```bash
char baru[100]="/home/";
strcat(baru, user);
strcat(baru, "/shift2/drakor/");
strcat(baru, cat);
strcat(baru, "/");
strcat(baru, name);
execlp("/bin/cp","cp", pathlama, baru, NULL);
```

kemudian pada task terakhir kita disuruh membuat data.txt yang sudah diurutkan berdasarkan tahun rilis. dengan memanggil fungsi tersebut akan men-generate data.txt disetiap genre
```bash
void listFilesRecursively(char *path);
void listFilesRecursively2(char *path);
void listFilesRecursively3(char *path);
```
# Soal 3
pada task pertama kita disuruh untuk membuat directory darat setelah itu akan membuat directory air secara otomatis
```bash
else if (child_id5 == 0)
{
    char path[]="/home/";
    strcat(path, user);
    strcat(path, "/modul2/darat");
    char *argv[] = {"mkdir", "-p", path, NULL};
    execv("/bin/mkdir", argv);
}
else 
{
    while((wait(&status))>0);
    sleep(3);
    char path[]="/home/";
    strcat(path, user);
    strcat(path, "/modul2/air");
    char *argv[] = {"mkdir", "-p", path, NULL};
    execv("/bin/mkdir", argv);                
}
```
kemudian kita dapat mengekstrak file yang berekstensi zip 
```bash
while((wait(&status))>0);
char path[]="/home/";
strcat(path, user);
strcat(path, "/modul2");
execl("/bin/unzip","unzip", "animal.zip", "-d", path, NULL);
```
selanjut kita dapat mengklasifikan hewan tersebut termasuk air atau darat kemudian kita masukkan kedalam directory sesuai dengan hasil klasifikasinya. namun jika hewan tersebut tidak dapat diklasifikasi kita dapat menghapus secara otomatis melalui program 

jika pada ep->d_name terdapat kata-kata "darat" maka 
```bash
char pathFileLama[100] = "/home/";
strcat(pathFileLama, user);
strcat(pathFileLama, "/modul2/animal/");
strcat(pathFileLama, ep->d_name);

char baru[100] = "/home/";
strcat(baru, user);
strcat(baru, "/modul2/darat/");
strcat(baru, ep->d_name);
                           
size_t len = 0 ;
char a[100] ;
char b[100] ;
strcpy(a, pathFileLama);
strcpy(b, baru);
char buffer[BUFSIZ] = { '\0' } ;

FILE* in = fopen( a, "rb" ) ;
FILE* out = fopen( b, "wb" ) ;

if( in == NULL || out == NULL )
{
    perror( "An error occured while opening files!!!" ) ;
    in = out = 0 ;
}
else 
{
while( (len = fread( buffer, BUFSIZ, 1, in)) > 0 )
{
    fwrite( buffer, BUFSIZ, 1, out ) ;
}
fclose(in) ;
fclose(out) ;
remove(a);
}
```
namun jika ep->d_name terdapat kata-kata "air" maka 
```bash
char pathFileLama[100] = "/home/";
strcat(pathFileLama, user);
strcat(pathFileLama, "/modul2/animal/");
strcat(pathFileLama, ep->d_name);

char baru[100] = "/home/";
strcat(baru, user);
strcat(baru, "/modul2/air/");
strcat(baru, ep->d_name);
                           
size_t len = 0 ;
char a[100] ;
char b[100] ;
strcpy(a, pathFileLama);
strcpy(b, baru);
char buffer[BUFSIZ] = { '\0' } ;

FILE* in = fopen( a, "rb" ) ;
FILE* out = fopen( b, "wb" ) ;

if( in == NULL || out == NULL )
{
    perror( "An error occured while opening files!!!" ) ;
    in = out = 0 ;
}
else 
{
    while( (len = fread( buffer, BUFSIZ, 1, in)) > 0 )
    {
        fwrite( buffer, BUFSIZ, 1, out ) ;
    }
                            
    fclose(in) ;
    fclose(out) ;
    remove(a);
}
```
namun jika tidak maka file tersebut akan dihapus 
```bash
char pathFileLama[100] = "/home/";
strcat(pathFileLama, user);
strcat(pathFileLama, "/modul2/animal/");
strcat(pathFileLama, ep->d_name);
remove(pathFileLama);
```
namun setelah kita telsuri pada file darat kita disuruh untuk menghapus file burung yang ada pada directory darat 
```bash
if(dp2 != NULL)
{
    int count = 0;
    while((ep2=readdir(dp2)))
    {
        if((strcmp(ep2->d_name, ".")== 0) ||(strcmp(ep2->d_name, "..")== 0)) continue;
        if(strstr(ep2->d_name, "bird"))
        {
            char pathFileLama[100] = "/home/";
            strcat(pathFileLama, user);
            strcat(pathFileLama, "/modul2/darat/");
            strcat(pathFileLama, ep2->d_name);
            remove(pathFileLama);
        }
    }
    closedir(dp2);
}
```
pada task terakhir kita membuat program yang bisa men-generate list.txt pada directory air yang manna list tersebut berisi apa aja yang berada di air dengan format UID 
```bash
char namaFile[100];
strcpy(namaFile, user);
strcat(namaFile, "_");
r = stat(pathFileLama, &fs);
if(r == -1) puts("huh gbs");

if((fs.st_mode & S_IRUSR)&&(fs.st_mode & S_IWUSR)&&(fs.st_mode & S_IXUSR)) strcat(namaFile, "rwx");
else if((fs.st_mode & S_IRUSR)&&(fs.st_mode & S_IWUSR)) strcat(namaFile, "rw-");
else if((fs.st_mode & S_IRUSR)&&(fs.st_mode & S_IXUSR)) strcat(namaFile, "r-x");
else if((fs.st_mode & S_IWUSR)&&(fs.st_mode & S_IXUSR)) strcat(namaFile, "-wx");
else if(fs.st_mode & S_IRUSR) strcat(namaFile, "r--");
else if(fs.st_mode & S_IWUSR) strcat(namaFile, "-w-");
else if(fs.st_mode & S_IXUSR) strcat(namaFile, "--x");  

strcat(namaFile, "_");
strcat(namaFile, ep3->d_name);

char pathFile[100] = "/home/";
strcat(pathFile, user);
strcat(pathFile, "/modul2/air/list.txt");
FILE *file = fopen(pathFile, "a");
fprintf(file, "%s\n", namaFile);
```

# Kendala
- Tidak Ada Asistensi Individu, hiks
- Harus membuka man bash berkali kali karena tidak disediakan di modul
- sulit mendebuggs karena perbedaan menggunakan visual studio code dengan menggunakan gedit 
- Bingung harus ngapain, search di googlenya juga susah
- Soal no 1 kurang jelas untuk penggunaan daemon atau tidak. Tapi kelompok kita pakai dan aman
- Soal no 2 banyak yang curang karena hard-code (bikin file satu"). kelommpok kita tidak 😎

# Catatan
Tidak soal yang Revisi, alhamdulillah
