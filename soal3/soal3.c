#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<stdio.h>
#include<wait.h>
#include<string.h>
#include<pwd.h>
#include<sys/stat.h>
#include<dirent.h>
char user[100];

int main()
{
    int status;
    pid_t child_id;
    child_id = fork();
    strcpy(user, getenv("USER"));
    if(child_id < 0) exit(EXIT_FAILURE);
    else if (child_id == 0)
    {
        int status;
        pid_t child_id2;
        child_id2 = fork();
        if(child_id2 < 0) exit(EXIT_FAILURE);
        else if (child_id2 == 0)
        {
            int status;
            pid_t child_id3;
            child_id3 = fork();
            if(child_id3 < 0) exit(EXIT_FAILURE);
            else if (child_id3 == 0)
            {
                int status;
                pid_t child_id4;
                child_id4 = fork();
                if(child_id4 < 0) exit(EXIT_FAILURE);
                else if (child_id4 == 0)
                {
                    int status;
                    pid_t child_id5;
                    child_id5 = fork();
                    if(child_id5 < 0) exit(EXIT_FAILURE);
                    else if (child_id5 == 0)
                    {
                        char path[]="/home/";
                        strcat(path, user);
                        strcat(path, "/modul2/darat");
                        char *argv[] = {"mkdir", "-p", path, NULL};
                        execv("/bin/mkdir", argv);
                    }
                    else 
                    {
                        while((wait(&status))>0);
                        sleep(3);
                        char path[]="/home/";
                        strcat(path, user);
                        strcat(path, "/modul2/air");
                        char *argv[] = {"mkdir", "-p", path, NULL};
                        execv("/bin/mkdir", argv);
                        
                    }
                }
                else 
                {
                    while((wait(&status))>0);
                    char path[]="/home/";
                    strcat(path, user);
                    strcat(path, "/modul2");
                    execl("/bin/unzip","unzip", "animal.zip", "-d", path, NULL);
                    
                }
            }
            else 
            {
                while((wait(&status))>0);
                DIR *dp;
                struct dirent *ep;
                char path[] = "/home/";
                strcat(path, user);
                strcat(path, "/modul2/animal");
                dp = opendir(path);
                if(dp != NULL)
                {
                    int count = 0;
                    while((ep=readdir(dp)))
                    {
                        if((strcmp(ep->d_name, ".")== 0) ||(strcmp(ep->d_name, "..")== 0)) continue;
                        if(strstr(ep->d_name, "darat"))
                        {
                            char pathFileLama[100] = "/home/";
                            strcat(pathFileLama, user);
                            strcat(pathFileLama, "/modul2/animal/");
                            strcat(pathFileLama, ep->d_name);

                            char baru[100] = "/home/";
                            strcat(baru, user);
                            strcat(baru, "/modul2/darat/");
                            strcat(baru, ep->d_name);
                           
                            size_t len = 0 ;
                            char a[100] ;
                            char b[100] ;
                            strcpy(a, pathFileLama);
                            strcpy(b, baru);
                            char buffer[BUFSIZ] = { '\0' } ;

                            FILE* in = fopen( a, "rb" ) ;
                            FILE* out = fopen( b, "wb" ) ;

                            if( in == NULL || out == NULL )
                            {
                                perror( "An error occured while opening files!!!" ) ;
                                in = out = 0 ;
                            }
                            else 
                            {
                                while( (len = fread( buffer, BUFSIZ, 1, in)) > 0 )
                                {
                                    fwrite( buffer, BUFSIZ, 1, out ) ;
                                }
                            
                                fclose(in) ;
                                fclose(out) ;
                                remove(a);
                            }
                        }
                        else if(strstr(ep->d_name, "air"))
                        {
                            char pathFileLama[100] = "/home/";
                            strcat(pathFileLama, user);
                            strcat(pathFileLama, "/modul2/animal/");
                            strcat(pathFileLama, ep->d_name);

                            char baru[100] = "/home/";
                            strcat(baru, user);
                            strcat(baru, "/modul2/air/");
                            strcat(baru, ep->d_name);
                           
                            size_t len = 0 ;
                            char a[100] ;
                            char b[100] ;
                            strcpy(a, pathFileLama);
                            strcpy(b, baru);
                            char buffer[BUFSIZ] = { '\0' } ;

                            FILE* in = fopen( a, "rb" ) ;
                            FILE* out = fopen( b, "wb" ) ;

                            if( in == NULL || out == NULL )
                            {
                                perror( "An error occured while opening files!!!" ) ;
                                in = out = 0 ;
                            }
                            else 
                            {
                                while( (len = fread( buffer, BUFSIZ, 1, in)) > 0 )
                                {
                                    fwrite( buffer, BUFSIZ, 1, out ) ;
                                }
                            
                                fclose(in) ;
                                fclose(out) ;
                                remove(a);
                            }
                        }
                        else
                        {
                            char pathFileLama[100] = "/home/";
                            strcat(pathFileLama, user);
                            strcat(pathFileLama, "/modul2/animal/");
                            strcat(pathFileLama, ep->d_name);
                            remove(pathFileLama);

                        }
                    }
                    while((ep=readdir(dp)))
                    {
                        if((strcmp(ep->d_name, ".")== 0) ||(strcmp(ep->d_name, "..")== 0)) continue;
                        char pathFileLama[100] = "/home/";
                        strcat(pathFileLama, user);
                        strcat(pathFileLama, "/modul2/animal/");
                        strcat(pathFileLama, ep->d_name);
                        remove(pathFileLama);
                    }
                    closedir(dp);
                }
                else perror("gagal ahaha\n");
                
            }
        }
        else 
        {
            while((wait(&status))>0);
            DIR *dp2;
            struct dirent *ep2;
            char path[] = "/home/";
            strcat(path, user);
            strcat(path, "/modul2/darat");
            dp2 = opendir(path);
            if(dp2 != NULL)
            {
                int count = 0;
                while((ep2=readdir(dp2)))
                {
                    if((strcmp(ep2->d_name, ".")== 0) ||(strcmp(ep2->d_name, "..")== 0)) continue;
                    if(strstr(ep2->d_name, "bird"))
                    {
                        char pathFileLama[100] = "/home/";
                        strcat(pathFileLama, user);
                        strcat(pathFileLama, "/modul2/darat/");
                        strcat(pathFileLama, ep2->d_name);
                        remove(pathFileLama);
                    }
                }
                closedir(dp2);
            }
            
        }
    }
    else 
    {
        while((wait(&status))>0);
        while((wait(&status))>0);
        DIR *dp3;
        struct dirent *ep3;
        char path[] = "/home/";
        strcat(path, user);
        strcat(path, "/modul2/air");
        dp3 = opendir(path);
        if(dp3 != NULL)
        {
            while((ep3 = readdir(dp3)))
            {
                if((strcmp(ep3->d_name, ".")== 0) ||(strcmp(ep3->d_name, "..")== 0)) continue;
                struct stat fs;
                int r;
                char pathFileLama[100] = "/home/";
                strcat(pathFileLama, user);
                strcat(pathFileLama, "/modul2/air/");
                strcat(pathFileLama, ep3->d_name);

                char namaFile[100];
                strcpy(namaFile, user);
                strcat(namaFile, "_");

                r = stat(pathFileLama, &fs);
                if(r == -1) puts("huh gbs");

                if((fs.st_mode & S_IRUSR)&&(fs.st_mode & S_IWUSR)&&(fs.st_mode & S_IXUSR)) strcat(namaFile, "rwx");
                else if((fs.st_mode & S_IRUSR)&&(fs.st_mode & S_IWUSR)) strcat(namaFile, "rw-");
                else if((fs.st_mode & S_IRUSR)&&(fs.st_mode & S_IXUSR)) strcat(namaFile, "r-x");
                else if((fs.st_mode & S_IWUSR)&&(fs.st_mode & S_IXUSR)) strcat(namaFile, "-wx");
                else if(fs.st_mode & S_IRUSR) strcat(namaFile, "r--");
                else if(fs.st_mode & S_IWUSR) strcat(namaFile, "-w-");
                else if(fs.st_mode & S_IXUSR) strcat(namaFile, "--x");  

                strcat(namaFile, "_");
                strcat(namaFile, ep3->d_name);

                char pathFile[100] = "/home/";
                strcat(pathFile, user);
                strcat(pathFile, "/modul2/air/list.txt");
                FILE *file = fopen(pathFile, "a");
                fprintf(file, "%s\n", namaFile);

            }
            closedir(dp3);
        }
        
    }
}
