#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<stdio.h>
#include<wait.h>
#include<string.h>
#include<pwd.h>
#include<sys/stat.h>
#include<dirent.h>
char user[100];
char category[100];

void listFilesRecursively(char *path);
void listFilesRecursively2(char *path);
void listFilesRecursively3(char *path);

int main()
{
    int status;
    pid_t child_id;
    strcpy(user, getenv("USER"));
    child_id = fork();
    
    if(child_id < 0) exit(EXIT_FAILURE);
    else if (child_id == 0)
    {
        //kodingan 1 mkdir drakor
        char path[]="/home/";
        strcat(path, user);
        strcat(path, "/shift2/drakor");
        char *argv[] = {"mkdir", "-p", path, NULL};
        execv("/bin/mkdir", argv);
    }
    else
    {
        while((wait(&status))>0);
        int status;
        pid_t child_id;
        child_id = fork();
        if(child_id < 0) exit(EXIT_FAILURE);
        else if (child_id == 0)
        {
            //kodingan 2 unzip drakor
            char path[]="/home/";
            strcat(path, user);
            strcat(path, "/shift2/drakor");
            execl("/bin/unzip","unzip", "drakor.zip", "-d", path, NULL);
        }
        else
        {
            while((wait(&status))>0);
            int status;
            pid_t child_id;
            child_id = fork();
            if(child_id < 0) exit(EXIT_FAILURE);
            else if (child_id == 0)
            {
                //kodingan 3 iterasi per file
                int status;
                DIR *dp;
                struct dirent *ep;
                char path[] = "/home/";
                strcat(path, user);
                strcat(path, "/shift2/drakor");
                dp = opendir(path);
                if(dp != NULL)
                {
                    while((ep=readdir(dp)))
                    {
                        if((strcmp(ep->d_name, ".")== 0) ||(strcmp(ep->d_name, "..")== 0)) continue;
                        char name[100];
                        strcpy(name, ep->d_name);
                        int simbol = 0; int print = 1;
                        for(int i = 0; i < strlen(name); i++)
                        {
                            if(name[i] == ';') simbol++;
                        }
                        if(simbol == 0)
                        {
                            //remove folder
                            pid_t f;
                            f = fork();
                            if(f < 0) exit(EXIT_FAILURE);
                            else if(f == 0)
                            {
                                char pathlama[100]="/home/";
                                strcat(pathlama, user);
                                strcat(pathlama, "/shift2/drakor/");
                                strcat(pathlama, name);
                                //printf("remove: %s\n", pathlama);
                                execl("/bin/rm", "rm", "-r", pathlama, NULL);
                            }
                            else{}
                        }
                        else if(simbol == 2)
                        {
                            int count = 0, print = 1, j = 0;
                            char cat[100]= "";
                            for(int i = 0; i < strlen(name); i++)
                            {
                                if(name[i]=='.') print = 0;
                                if(count == 2 && print == 1)
                                {
                                    cat[j] = name[i];
                                    j++;
                                }
                                if(name[i] == ';') count++;
                            }
                            pid_t child_id;
                            child_id = fork();
                            if(child_id < 0) exit(EXIT_FAILURE);
                            else if(child_id == 0)
                            {
                                char path[]="/home/";
                                strcat(path, user);
                                strcat(path, "/shift2/drakor/");
                                strcat(path, cat);
                                execlp("/bin/mkdir","mkdir", "-p", path, NULL);
                            }
                            else
                            {
                                while((wait(&status))>0);
                                pid_t child_ida;
                                child_ida=fork();
                                if(child_ida<0) exit(EXIT_FAILURE);
                                else if(child_ida == 0)
                                {
                                    char pathlama[100]="/home/";
                                    strcat(pathlama, user);
                                    strcat(pathlama, "/shift2/drakor/");
                                    strcat(pathlama, name);

                                    char baru[100]="/home/";
                                    strcat(baru, user);
                                    strcat(baru, "/shift2/drakor/");
                                    strcat(baru, cat);
                                    strcat(baru, "/");
                                    strcat(baru, name);
                                    execlp("/bin/cp","cp", pathlama, baru, NULL);
                                }
                                else
                                {
                                    while((wait(&status))>0);
                                    char pathlama[100]="/home/";
                                    strcat(pathlama, user);
                                    strcat(pathlama, "/shift2/drakor/");
                                    strcat(pathlama, name);
                                    remove(pathlama);
                                }
                            }
                        }
                        else if(simbol == 4)
                        {
                            char cadangan[100];
                            strcpy(cadangan, name);
                            //puts("simbol 4");
                            const char s[2] = "_";
                            char *pisah;
                            pisah = strtok(name, s);
                            while(pisah != NULL)
                            {
                                if(!(strstr(pisah,"png")))
                                {
                                    char first[100];
                                    strcpy(first, pisah);
                                    strcat(first, ".png");
                                    //printf("%s\n", first);
                                    int count = 0, print = 1, j = 0;
                                    char cat[100]= "";
                                    for(int i = 0; i < strlen(name); i++)
                                    {
                                        if(name[i]=='.') print = 0;
                                        if(count == 2 && print == 1)
                                        {
                                            cat[j] = name[i];
                                            j++;
                                        }
                                        if(name[i] == ';') count++;
                                    }
                                    pid_t child_idda;
                                    child_idda = fork();
                                    if(child_idda<0)exit(EXIT_FAILURE);
                                    else if(child_idda == 0)
                                    {
                                        pid_t child_x;
                                        child_x = fork();
                                        if(child_x<0) exit(EXIT_FAILURE);
                                        else if(child_x == 0)
                                        {
                                            //puts("haha1");
                                            char path[]="/home/";
                                            strcat(path, user);
                                            strcat(path, "/shift2/drakor/");
                                            strcat(path, cat);
                                            execlp("/bin/mkdir","mkdir", "-p", path, NULL);
                                        }
                                        else
                                        {
                                            while((wait(&status))>0);
                                            //puts("haha2");
                                            char pathlama[100]="/home/";
                                            strcat(pathlama, user);
                                            strcat(pathlama, "/shift2/drakor/");
                                            strcat(pathlama, cadangan);

                                            char baru[100]="/home/";
                                            strcat(baru, user);
                                            strcat(baru, "/shift2/drakor/");
                                            strcat(baru, cat);
                                            strcat(baru, "/");
                                            strcat(baru, first);
                                            //printf("pathlama : %s\n", pathlama);
                                            //printf("baru : %s\n", baru);
                                            execlp("/bin/cp","cp", pathlama, baru, NULL);
                                        }
                                    }
                                    else
                                    {
                                    }
                                }
                                else
                                {
                                    //printf("%s\n", pisah);
                                    int count = 0, print = 1, j = 0;
                                    char cat[100]= "";
                                    for(int i = 0; i < strlen(pisah); i++)
                                    {
                                        if(pisah[i]=='.') print = 0;
                                        if(count == 2 && print == 1)
                                        {
                                            cat[j] = pisah[i];
                                            j++;
                                        }
                                        if(pisah[i] == ';') count++;
                                    }
                                    pid_t child_iddw;
                                    child_iddw = fork();
                                    if(child_iddw<0)exit(EXIT_FAILURE);
                                    else if(child_iddw == 0)
                                    {
                                        pid_t childs;
                                        childs = fork();
                                        if(childs < 0)exit(EXIT_FAILURE);
                                        else if(childs == 0)
                                        {
                                            //puts("haha3");
                                            char path[]="/home/";
                                            strcat(path, user);
                                            strcat(path, "/shift2/drakor/");
                                            strcat(path, cat);
                                            execlp("/bin/mkdir","mkdir", "-p", path, NULL);
                                        }
                                        else
                                        {
                                            while((wait(&status))>0);
                                            //puts("haha4");
                                            char pathlama[100]="/home/";
                                            strcat(pathlama, user);
                                            strcat(pathlama, "/shift2/drakor/");
                                            strcat(pathlama, cadangan);

                                            char baru[100]="/home/";
                                            strcat(baru, user);
                                            strcat(baru, "/shift2/drakor/");
                                            strcat(baru, cat);
                                            strcat(baru, "/");
                                            strcat(baru, pisah);
                                            //printf("pathlama2 : %s\n", pathlama);
                                            //printf("baru2 : %s\n", baru);
                                            execlp("/bin/cp","cp", pathlama, baru, NULL);
                                        }
                                         
                                    }
                                    else
                                    {
                                        
                                    }
                                }
                                pisah = strtok(NULL, s);
                            }
                            
                        }

                    }
                }
                
            }
            else
            {
                while((wait(&status))>0);
                int status;
                pid_t child_id;
                child_id = fork();
                if(child_id < 0) exit(EXIT_FAILURE);
                else if (child_id == 0)
                {
                    int status;
                    DIR *dp;
                    struct dirent *ep;
                    char path[] = "/home/";
                    strcat(path, user);
                    strcat(path, "/shift2/drakor");
                    dp = opendir(path);
                    if(dp != NULL)
                    {
                        while((ep=readdir(dp)))
                        {
                            if(strstr(ep->d_name, ".png"))
                            {
                                //puts(ep->d_name);
                                pid_t ff;
                                ff = fork();
                                if(ff < 0) exit(EXIT_FAILURE);
                                else if(ff == 0)
                                {
                                    char pathlama[100]="/home/";
                                    strcat(pathlama, user);
                                    strcat(pathlama, "/shift2/drakor/");
                                    strcat(pathlama, ep->d_name);
                                    //printf("remove: %s\n", pathlama);
                                    execl("/bin/rm", "rm", "-r", pathlama, NULL);
                                }
                                else{}
                            }
                        }
                    }

                }
                else
                {
                    while((wait(&status))>0);
                    int status;
                    pid_t child_id;
                    child_id = fork();
                    if(child_id < 0) exit(EXIT_FAILURE);
                    else if (child_id == 0)
                    {
                        pid_t child_id;
                        child_id = fork();
                        if(child_id < 0) exit(EXIT_FAILURE);
                        else if (child_id == 0)
                        {
                            pid_t child_id;
                            child_id = fork();
                            if(child_id < 0) exit(EXIT_FAILURE);
                            else if (child_id == 0)
                            {
                                pid_t child_id;
                                child_id = fork();
                                if(child_id < 0) exit(EXIT_FAILURE);
                                else if (child_id == 0)
                                {
                                    char path[]="/home/";
                                    strcat(path, user);
                                    strcat(path, "/shift2/drakor");
                                    listFilesRecursively(path);
                                }
                                else
                                {
                                    while((wait(&status))>0);
                                    char path[]="/home/";
                                    strcat(path, user);
                                    strcat(path, "/shift2/drakor");
                                    listFilesRecursively2(path);
                                }

                            }
                            else
                            {
                                while((wait(&status))>0);
                                char path[]="/home/";
                                strcat(path, user);
                                strcat(path, "/shift2/drakor");
                                listFilesRecursively3(path);
                            }

                        }
                        else
                        {
                           
                        }
                    }
                    else
                    {
                        
                    }
                }
            }
        }
    }
}

void listFilesRecursively3(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    if (!dir)
        return;
    int beda = 0;
    while ((dp = readdir(dir)) != NULL)
    {
        if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        if((strstr(dp->d_name, ".txt") != 0) && (strstr(dp->d_name, "data")== 0))
        {
            //printf("ini txt? %s\n", dp->d_name);
            char hapus[100]="";
            strcpy(hapus, basePath);
            strcat(hapus, "/");
            strcat(hapus, dp->d_name);
            //printf("hapus :%s\n", hapus);
            remove(hapus);

            continue;
        }
        
        strcpy(path, basePath);
        strcat(path, "/");
        strcat(path, dp->d_name);

        listFilesRecursively3(path);
    }

    closedir(dir);
}

void listFilesRecursively2(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    if (!dir)
        return;
    int beda = 0;
    while ((dp = readdir(dir)) != NULL)
    {
        if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        if(strstr(dp->d_name, ".txt") != 0)
        {
            char lol[100]="";
            for(int i = 0; i < strlen(dp->d_name); i++)
            {
                if(dp->d_name[i] == '.') break;
                lol[i] = dp->d_name[i];
                //printf("%c\n", dp->d_name[i]);
            }

            char pathFile[100] = "/home/";
            strcat(pathFile, user);
            strcat(pathFile, "/shift2/drakor/");
            strcat(pathFile, lol);
            strcat(pathFile, "/");
            strcat(pathFile, lol);
            strcat(pathFile, ".txt");

            char baru[100] = "/home/";
            strcat(baru, user);
            strcat(baru, "/shift2/drakor/");
            strcat(baru, lol);
            strcat(baru, "/data.txt");

            // printf("pathFile : %s\n", pathFile);
            // printf("baru : %s\n", baru);

            char strFileName[100];
            char strFileSummary[100];
            strcpy(strFileName, pathFile);
            strcpy(strFileSummary, baru);

            char strTempData[100];
            char **strData = NULL; // String List
            int i, j;
            int noOfLines = 0;

            FILE * ptrFileLog = NULL;
            FILE * ptrSummary = NULL;

            if ( (ptrFileLog = fopen(strFileName, "r")) == NULL ) {
                fprintf(stderr,"Error: Could not open %s\n",strFileName);
                //return 1;
            }
            if ( (ptrSummary = fopen(strFileSummary, "a")) == NULL ) {
                fprintf(stderr,"Error: Could not open %s\n",strFileSummary);
                //return 1;
            }

            // Read and store in a string list.
            while(fgets(strTempData, 100, ptrFileLog) != NULL) 
            {
                // Remove the trailing newline character
                if(strchr(strTempData,'\n'))
                    strTempData[strlen(strTempData)-1] = '\0';
                strData = (char**)realloc(strData, sizeof(char**)*(noOfLines+1));
                strData[noOfLines] = (char*)calloc(100,sizeof(char));
                strcpy(strData[noOfLines], strTempData);
                noOfLines++;
            }
            // Sort the array.
                for(i= 0; i < (noOfLines - 1); ++i) {
                    for(j = 0; j < ( noOfLines - i - 1); ++j) {
                        if(strcmp(strData[j], strData[j+1]) > 0) {
                            strcpy(strTempData, strData[j]);
                            strcpy(strData[j], strData[j+1]);
                            strcpy(strData[j+1], strTempData);
                        }
                    }
                }
                // Write it to outfile. file.
                fprintf(ptrSummary,"kategori: %s\n\n",lol);
                for(i = 0; i < noOfLines-1; i++)
                {
                    //puts(strData[i]);
                    char s1[100]="";
                    char s2[100]="";
                    char s3[100]="";
                    char nnama[100];
                    int count = 0,x = 0, y = 0, z = 0;
                    strcpy(nnama, strData[i]);
                    //printf("nnama : %s\n", nnama);
                    for(int j = 0; j < strlen(nnama); j++)
                    {
                        if(nnama[j]==';') 
                        {
                            count++;
                            continue;
                        }
                        else if(count == 0)
                        {
                            s1[x] = nnama[j];
                            x++;
                        }
                        else if(count == 1)
                        {
                            s2[y] = nnama[j];
                            y++;
                        }
                        else if(count == 2)
                        {
                            s3[z] = nnama[j];
                            z++;
                        }

                    }
                    //printf("s1: %s, s2: %s, s3: %s\n", s1, s2, s3);
                    char baris1[100]="nama : ";
                    char baris2[100]="rilis tahun : ";

                    strcat(baris1, s1);
                    strcat(baris2, s2);
                    fprintf(ptrSummary,"\n%s\n%s\n",baris1, baris2);
                }
                // free each string
                for(i = 0; i < noOfLines; i++)
                    free(strData[i]);
                // free string list.
                free(strData);
                fclose(ptrFileLog);
                fclose(ptrSummary);

        }
        strcpy(path, basePath);
        strcat(path, "/");
        strcat(path, dp->d_name);

        listFilesRecursively2(path);
    }

    closedir(dir);
}



void listFilesRecursively(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    if (!dir)
        return;
    int beda = 0;
    while ((dp = readdir(dir)) != NULL)
    {
        if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        if(strstr(dp->d_name, ".txt") != 0)
        {
            //printf("ini txt? %s\n", dp->d_name);
            continue;
        }
        if((!(strstr(dp->d_name, ".png")))&&(!(strstr(dp->d_name, ".txt"))))
        {
            strcpy(category, dp->d_name);
            beda = 1;
        }
        else beda = 0;
        if(beda == 1)
        {
            char pathFile[100] = "/home/";
            strcat(pathFile, user);
            strcat(pathFile, "/shift2/drakor/");
            strcat(pathFile, category);
            strcat(pathFile, "/");
            strcat(pathFile, category);
            strcat(pathFile, ".txt");

            FILE *file = fopen(pathFile, "a+");
            //puts(pathFile);
            //printf("atas %s\n", dp->d_name);
            fprintf(file, "kategori: %s\n", category);
        }
        else if(beda == 0)
        {
            char pathFile[100] = "/home/";
            strcat(pathFile, user);
            strcat(pathFile, "/shift2/drakor/");
            strcat(pathFile, category);
            strcat(pathFile, "/");
            strcat(pathFile, category);
            strcat(pathFile, ".txt");

            FILE *file = fopen(pathFile, "a+");
            //puts(pathFile);
            //printf("bawah %s\n", dp->d_name);
            char s1[100]="";
            char s2[100]="";
            char s3[100]="";
            char nnama[100];
            strcpy(nnama, dp->d_name);
            int count = 0,x = 0, y = 0, z = 0;
            for(int i = 0; i < strlen(nnama); i++)
            {
                if(nnama[i]==';') 
                {
                    count++;
                    continue;
                }
                else if(count == 0)
                {
                    s1[x] = nnama[i];
                    x++;
                }
                else if(count == 1)
                {
                    s2[y] = nnama[i];
                    y++;
                }
                else if(count == 2)
                {
                    s3[z] = nnama[i];
                    z++;
                }
            }
            char namaFile[100] = "";
            strcat(namaFile, s2);
            strcat(namaFile, ";");
            strcat(namaFile, s1);
            strcat(namaFile, ";");
            strcat(namaFile, s3);
            strcat(namaFile, ";");

            fprintf(file, "%s\n", namaFile);
        }

        strcpy(path, basePath);
        strcat(path, "/");
        strcat(path, dp->d_name);

        listFilesRecursively(path);
    }

    closedir(dir);
}
